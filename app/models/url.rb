class Url < ApplicationRecord
  include UrlValidator

  validates :token, :original_link, presence: true
  validates :token, uniqueness: true
  validates :original_link, uniqueness: { scope: :custom }
  validates :original_link, url: true
  validates :token, format: { with: /\A[a-zA-Z0-9\-\_]+\z/, message: 'only allows letters and numbers'}
  before_validation :generate_token, unless: :token?

private
  def generate_token
    token_len = 3
    3.times do |i|
      token = SecureRandom.urlsafe_base64 token_len+i
      if Url.find_by_token(token).blank?
        self.token = token
        break
      end
    end
    if self.token.blank?
      errors[:base] << "Out of token."
    end
  end

end
