class UrlsController < ApplicationController

  def new
    @url = Url.new
  end

  def create
    @url = Url.new(url_params)

    found_url = Url.find_by_original_link_and_custom(params[:url][:original_link], params[:url][:custom])

    if found_url.present?
      if params[:url][:custom] == 'true'
        flash[:alert] = "The link has already been shortened."
        render :new
      else
        redirect_to root_path, notice: "Your shorten url: #{root_url}#{found_url.token}"
      end
    else
      if @url.save
        redirect_to root_path, notice: "Your shorten url: #{root_url}#{@url.token}"
      else
        render :new
      end
    end
  end

  def show
    if params[:token].present?
      url = Url.find_by_token(params[:token])
      if url.present?
        redirect_to url.original_link
      else
        redirect_to root_path, alert: "Sorry, the url does not exist."
      end
    else
      redirect_to root_path
    end
  end

private
  def url_params
    params.require(:url).permit(:token, :original_link, :custom)
  end

end
