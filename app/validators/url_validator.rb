module UrlValidator
  class UrlValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      uri = URI.parse(value)
      if value.blank? || !uri.is_a?(URI::HTTP) || uri.host.blank? || !uri.host.include?('.')
        record.errors.add(attribute, "is not a valid URL")
      end
    end
  end
end