class CreateUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :urls do |t|
      t.string :token
      t.string :original_link
      t.boolean :custom, default: false

      t.timestamps
    end
  end
end
