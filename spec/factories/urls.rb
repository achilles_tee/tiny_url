FactoryBot.define do
  factory :url do
    original_link "https://www.google.com"
    custom false

    trait :custom do
      custom true
      token "MyString"
    end
  end
end
