require "rails_helper"

RSpec.describe Url, type: :model do
  
  describe "is valid with valid attributes" do
    before do
      @url = create(:url)
    end
    it { expect(@url).to be_valid }
  end

  describe "has a unique token" do
    before do
      url = create(:url)
    end
    it do
      url2 = build(:url, original_link: 'https://www.google.com')
      expect(url2).to_not be_valid 
    end
  end

  describe "has a unique orignal+custom" do
    before do
      url = create(:url)
    end
    it do
      url2 = build(:url, token: 'abc')
      expect(url2).to_not be_valid
      url3 = build(:url, original_link: "http://www.google.com")
      expect(url3).to be_valid
      url4 = build(:url, token: 'abc', custom: true)
      expect(url4).to be_valid
    end
  end

  describe "has a valid original_link url" do
    it do
      url2 = build(:url, original_link: '')
      expect(url2).to_not be_valid
      url3 = build(:url, original_link: 'https')
      expect(url3).to_not be_valid
      url4 = build(:url, original_link: 'aaa')
      expect(url4).to_not be_valid
      url5 = build(:url, original_link: 'https://')
      expect(url5).to_not be_valid
      url5 = build(:url, original_link: 'https://google')
      expect(url5).to_not be_valid
    end
  end

  describe "has a valid token" do
    it do
      '~`!@#$%^&*()+={}[]:";\',.?<>/'.split("").each do |char|
        url2 = build(:url, original_link: 'http://www.goo.gl', token: char)
        expect(url2).to_not be_valid
      end
    end
  end


end