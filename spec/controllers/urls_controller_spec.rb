require 'rails_helper'

RSpec.describe UrlsController, type: :controller do

  describe 'GET new' do
    context "successful" do
      it do
        get :new
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'POST create' do
    context "default" do
      context "successful" do
        it do
          post :create, params: { url: attributes_for(:url)}
          url = Url.find_by_original_link 'https://www.google.com'
          expect(response).to redirect_to(:root)
          expect(flash[:notice]).to eq("Your shorten url: #{root_url}#{url.token}")
        end
      end

      context "failed" do
        it do
          post :create, params: { url: attributes_for(:url, original_link: 'http://aaa')}
          expect(response).to render_template(:new)
        end
      end

      context "found" do
        before do
          @url = create(:url)
        end
        it do
          post :create, params: { url: attributes_for(:url)}
          expect(flash[:notice]).to eq("Your shorten url: #{root_url}#{@url.token}")
          expect(response).to redirect_to(:root)
        end
      end
    end

    context "custom" do
      context "successful" do
        it do
          post :create, params: { url: attributes_for(:url, :custom)}
          url = Url.find_by_original_link 'https://www.google.com'
          expect(response).to redirect_to(:root)
          expect(flash[:notice]).to eq("Your shorten url: #{root_url}#{url.token}")
        end
      end

      context "failed" do
        it do
          post :create, params: { url: attributes_for(:url, :custom, original_link: 'http://aaa') }
          expect(response).to render_template(:new)
        end
      end

      context "found" do
        before do
          @url = create(:url, :custom)
        end
        it do
          post :create, params: { url: attributes_for(:url, :custom)}
          expect(flash[:alert]).to eq("The link has already been shortened.")
          expect(response).to render_template(:new)
        end
      end
    end
  end

  describe 'GET show' do
    before do
      @url = create(:url)
    end
    context "found" do
      it do
        get :show, params: { token: @url.token }
        expect(response).to redirect_to(@url.original_link)
      end
    end
    context "not found" do
      it do
        get :show, params: { token: 'random' }
        expect(response).to redirect_to(:root)
        expect(flash[:alert]).to eq("Sorry, the url does not exist.")
      end
    end
  end

end
